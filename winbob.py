#kijkt of in row er alleen de speler en een lege veld is
def checkpossible(row , player):
    y=0
    for character in row:
        if character == 0 :
            y += 1
        if character != player and character != 0 :
            return False
    if y !=1 :
        return False
    else:
        return True
#kijkt welk coordinaat in row de 0 zit
def findthezero(row):
    x=0
    for character in row:
        if character == 0 :
            return x
        x += 1
#geeft de winnende zet voor de speler terug, horisontaal of verticaal als flipped = True en diagonaal en geflippt diagonaal
def pos(row,player,x,flipped=False,diagonal=False):
    if checkpossible(row, player) == True:
        if diagonal==True :
            #diagonaal
            if flipped==False:
                z = findthezero(row)
                return (z, z)
            #geflipt diagonal
            else:
                z = findthezero(row)
                y=(x-1-z)
                return (z , y)
            #horizontaal
        elif flipped ==False:
            return (x, findthezero(row))
            #verticaal
        else:
            return (findthezero(row),x)

#flips de lijst 90 graden
def flipfield(field):
    flippedrow = field
    flippedrow=list(zip(*flippedrow))
    return flippedrow
#main functie
def win(field, player):
    wins=[]
    x = 0
    y = 0
    #kijkt horizontaal
    for row in field:
        if pos(row,player,x) != None:
            wins.append(pos(row,player,x))
        x += 1
    #kijkt diagonaal
    for row in flipfield(field):
        if pos(row,player,y) != None:
            wins.append(pos(row,player,y,True))
        y += 1

    #kijkt verticaal
    newrow=[]
    oppositenewrow=[]
    #maakt de verticale rijen
    for i in range(x):
        #maakt de verticale rij
        z = field[i][i]
        newrow.append(z)
        #maakt de opposite verticale rij
        m=x-(i+1)
        yv= field[i][m]
        oppositenewrow.append(yv)
    #kijkt verticaal
    if pos(newrow,player,x,False,True) != None:
        wins.append(pos(newrow,player,x,False,True))
    #kijjkt opposite verticaal
    if pos(oppositenewrow,player,x,True,True) != None:
        wins.append(pos(oppositenewrow,player,x,True,True))
    return wins
player = 2
field = [[0,0,2],[0,2,0],[0,0,0]]
print (win(field,player))
