def checkwin(f, p):
    row = False
    col = False
    for i in range(3):
        row = row or (f[i][0] == f[i][1] == f[i][2] == p)
        col = col or (f[0][i] == f[1][i] == f[2][i] == p)        
    di1 = f[0][0] == f[1][1] == f[2][2] == p
    di2 = f[0][2] == f[1][1] == f[2][0] == p
    return row or col or di1 or di2

def win(f,p): # geeft een lijst met mogelijke zetten die resulteren in een fork voor speler p in veld f
    wins = []
    for y in range(3):
        for x in range(3):
            if f[y][x] == 0:
                b = deepcopy(f)
                b[y][x] = p
                if checkwin(b,p):
                    wins.append((y,x))
    return wins