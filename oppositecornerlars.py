#!/usr/bin/python3

def oppositecorner(player,field):
	if (field[0][0] == player) and (field[2][2] == 0):
		return(2,2)
	elif (field[2][2] == player) and (field[0][0] == 0):
		return(0,0)
	elif (field[0][2] == player) and (field[2][0] == 0):
		return(2,0)
	elif (field[2][0] == player) and (field[0][2] == 0):
		return(0,2)
	else:
		return False

player = 1
field = [[0,0,0],[0,0,0],[0,0,0]]
print (oppositecorner(player , field))
