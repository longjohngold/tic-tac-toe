def oppositecorner(field,player):
    if (field[0][0]== player or field[2][2]== player ) and (field[0][0] == 0 or field[2][2] ==0 ):
        if field[0][0]!=player:
            return (0,0)
        else:
            return (2,2)

    elif(field[0][2]== player or field[2][0]== player ) and (field[0][2] == 0 or field[2][0] ==0 ):
        if field[0][2]!=player:
            return (0,2)
        else:
            return (2,0)
    else:
        return False

field = [[1,2,1],[0,0,0],[1,1,0]]
print(oppositecorner(field,1))
