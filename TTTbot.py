class TTTbot:
    
    def __init__(self):               
        self.id = 1
        self.opponent = 2

    def empty(self, f):
        return [(y,x) for y,i in enumerate(f) for x,j in enumerate(i) if not j]

    def win(self, f, p):
        row = False
        col = False
        for i in range(3):
            row = row or (f[i][0] == f[i][1] == f[i][2] == p)
            col = col or (f[0][i] == f[1][i] == f[2][i] == p)        
        di1 = f[0][0] == f[1][1] == f[2][2] == p
        di2 = f[0][2] == f[1][1] == f[2][0] == p
        return row or col or di1 or di2

    def mm(self, f, p):  
        if self.win(f,self.id):
            return ((-1,-1),10)
        elif self.win(f,self.opponent):
            return ((-1,-1),-10)
        elif not self.empty(f):
            return ((-1,-1),0)

        moves = []
        for y,x in self.empty(f):
            f[y][x] = p
            m = self.mm(f,2 if p is 1 else 1)[1]
            f[y][x] = 0
            moves.append(((y,x),m))

        mm = max([l[1] for l in moves]) if p is self.id else min([l[1] for l in moves])
        return moves[[l[1] for l in moves].index(mm)]

    def move(self, **kwargs):
        p = kwargs['speler']
        self.id = p
        field = kwargs['array']        
        self.opponent = 2 if p is 1 else 1
        return (0,0) if len(self.empty(field)) is 9 else self.mm(field,self.id)[0]