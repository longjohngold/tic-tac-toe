# geeft een lijst met alle mogelijke zetten die een fork veroorzaken voor player p in veld f. 
# deze functie is afhankelijk van win()

def fork(f,p): 
    forks = []
    for y in range(3):
        for x in range(3):
            if f[y][x] == 0:
                b = deepcopy(f)
                b[y][x] = p
                wins = win(b,p)
                if len(wins) > 1:
                    forks.append((y,x))
                del(b)
    return forks