var oPlayer = “O”;
var xPlayer = “X”;

function emptyIndexies(board){
  return  board.filter(s => s != "O" && s != "X");
} 

#xs = [
# horizontal wins
#  [0, 1, 2],
#  [3, 4, 5],
#  [6, 7, 8],

# vertical wins
#  [0, 3, 6],
#  [1, 4, 7],
#  [2, 5, 8],

# diagonal wins
#  [0, 4, 8],
#  [6, 4, 2]]    


function winning(board, player){

if (
	(board[0][0] == player && board[0][1] == player && board[0][2] == player) ||
	(board[1][0] == player && board[1][1] == player && board[1][2] == player) ||
	(board[2][0] == player && board[2][1] == player && board[2][2] == player) ||
	(board[0][0] == player && board[1][0] == player && board[2][0] == player) ||
	(board[0][1] == player && board[1][1] == player && board[2][1] == player) ||
	(board[0][2] == player && board[1][2] == player && board[2][2] == player) ||
	(board[0][0] == player && board[1][1] == player && board[2][2] == player) ||
	(board[0][2] == player && board[1][1] == player && board[2][0] == player)
	) {
 		return true;
 	} else {
		return false;
	}
}

function minimax(newBoard, player){
	var availSpots = emptyIndexies(newBoard);

	if (winning(newBoard, oPlayer)){
    	return {score:-10};
	}
	else if (winning(newBoard, xPlayer)){
    	return {score:10};
	}
	else if (availSpots.length === 0){
  		return {score:0};
  	}

	var moves = [];

	for (var i = 0; i < availSpots.length; i++){

    	var move = {};
  		move.index = newBoard[availSpots[i]];

    	newBoard[availSpots[i]] = player;

    	if (player == aiPlayer){
      		var result = minimax(newBoard, oPlayer);
      		move.score = result.score;
    	}
    	else{
      		var result = minimax(newBoard, xPlayer);
      		move.score = result.score;
    	}
    	newBoard[availSpots[i]] = move.index;
    	moves.push(move);
	}

	var bestMove;
  	if(player === xPlayer){
    	var bestScore = -10000;
    	for(var i = 0; i < moves.length; i++){
      		if(moves[i].score > bestScore){
        		bestScore = moves[i].score;
        		bestMove = i;
      		}
    	}
	}else{
    	var bestScore = 10000;
    	for(var i = 0; i < moves.length; i++){
      		if(moves[i].score < bestScore){
        		bestScore = moves[i].score;
        		bestMove = i;
      		}
    	}
	}
	return moves[bestMove];
}

xs = [
# horizontal wins
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],

# vertical wins
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],

# diagonal wins
	[0, 4, 8],
	[6, 4, 2]]		

