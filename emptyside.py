def emptyside(field):
    if field[0][1] == 0:
        return (0,1)
    elif field[1][0] == 0:
        return (1,0)
    elif field[1][2] == 0:
        return (1,2)
    elif field[2][1] == 0:
        return (2,1)
    else:
        return False

field=[[0,0,2],[0,0,0],[1,0,2]]
print (emptyside(field))
