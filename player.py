from copy import deepcopy
import random

def empty(f):

    result = True
    for i in f: #de forfor loop checkt j in i de waarde 0 heeft.
        for j in i:
            if j != 0:
                result = False #Als het veld geen lege waarde heeft is het false is bot p2
                break #breakt de for loop af
    return result # returned het resul van de loop
#kijkt of in row er alleen de speler en een lege veld is
def checkpossible(row , player):
    y=0
    for character in row:
        if character == 0 :
            y += 1
        if character != player and character != 0 :
            return False
    if y !=1 :
        return False
    else:
        return True
#kijkt welk coordinaat in row de 0 zit
def findthezero(row):
    x=0
    for character in row:
        if character == 0 :
            return x
        x += 1
#geeft de winnende zet voor de speler terug, horisontaal of verticaal als flipped = True en diagonaal en geflippt diagonaal
def pos(row,player,x,flipped=False,diagonal=False):
    if checkpossible(row, player) == True:
        if diagonal==True :
            #diagonaal
            if flipped==False:
                z = findthezero(row)
                return (z, z)
            #geflipt diagonal
            else:
                z = findthezero(row)
                y=(x-1-z)
                return (z , y)
            #horizontaal
        elif flipped ==False:
            return (x, findthezero(row))
            #verticaal
        else:
            return (findthezero(row),x)

#flips de lijst 90 graden
def flipfield(field):
    flippedrow = field
    flippedrow=list(zip(*flippedrow))
    return flippedrow
#main functie
def win(field, player):
    wins=[]
    x = 0
    y = 0
    #kijkt horizontaal
    for row in field:
        if pos(row,player,x) != None:
            wins.append(pos(row,player,x))
        x += 1
    #kijkt diagonaal
    for row in flipfield(field):
        if pos(row,player,y) != None:
            wins.append(pos(row,player,y,True))
        y += 1

    #kijkt verticaal
    newrow=[]
    oppositenewrow=[]
    #maakt de verticale rijen
    for i in range(x):
        #maakt de verticale rij
        z = field[i][i]
        newrow.append(z)
        #maakt de opposite verticale rij
        m=x-(i+1)
        yv= field[i][m]
        oppositenewrow.append(yv)
    #kijkt verticaal
    if pos(newrow,player,x,False,True) != None:
        wins.append(pos(newrow,player,x,False,True))
    #kijjkt opposite verticaal
    if pos(oppositenewrow,player,x,True,True) != None:
        wins.append(pos(oppositenewrow,player,x,True,True))
    return wins

def fork(f,p): # geeft een lijst met mogelijke zetten die resulteren in een fork voor speler p in veld f
    forks = []
    for y in range(3):
        for x in range(3):
            if f[y][x] == 0:
                b = deepcopy(f)
                b[y][x] = p
                wins = win(b,p)
                if len(wins) > 1:
                    forks.append((y,x))
    return forks

def center(field): # geeft True of False als het middelste veld vrij is
    return field[1][1] == 0

def oppositecorner(field, player):
    if (field[0][0] == player) and (field[2][2] == 0):
        return(2,2)
    elif (field[2][2] == player) and (field[0][0] == 0):
        return(0,0)
    elif (field[0][2] == player) and (field[2][0] == 0):
        return(2,0)
    elif (field[2][0] == player) and (field[0][2] == 0):
        return(0,2)
    else:
        return False

def emptycorner(field):
  if field[0][0]==0:
    return (0,0)
  elif field[0][2]==0:
    return (0,2)
  elif field[2][0]==0:
    return (2,0)
  elif field[2][2]==0:
    return (2,2)
  else:
    return False

def emptyside(field):
    if field[0][1] == 0:
        return (0,1)
    elif field[1][0] == 0:
        return (1,0)
    elif field[1][2] == 0:
        return (1,2)
    elif field[2][1] == 0:
        return (2,1)
    else:
        return False

def epichotfix(field,p1,p2):
    if(field == [[0, 0, p2], [0, p1, 0], [p2, 0, 0]]) or (field == [[p2, 0, 0], [0, p1, 0], [0 , 0, p2]]):
        return (0, 1)

def calc(field, p1): # checkt alle bovenstaande functies in de juiste volgorde en geeft een zet terug waar mogelijk

    p2 = 2 if p1 is 1 else 1
    if epichotfix(field,p1,p2):
        return epichotfix(field,p1,p2)
    elif win(field, p1):
        return win(field, p1)[0]
    elif win(field, p2):
        return win(field, p2)[0]
    elif fork(field, p1):
        return fork(field, p1)[0]
    elif fork(field, p2):
        return fork(field, p2)[0]
    elif center(field):
        return (1,1)
    elif oppositecorner(field, p2):
        return oppositecorner(field, p2)
    elif emptycorner(field):
        return emptycorner(field)
    else:
        return emptyside(field)

def move(**kwargs):
    p = kwargs['speler']
    field = kwargs['array']
    if not empty(field):
        return calc(field, p)
    else:
        return (random.randint(0,2),random.randint(0,2))
#f = [[2, 0, 2], [0, 1, 0], [0, 0, 2]]

#print (move(array=f,speler=1))
